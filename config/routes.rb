Rails.application.routes.draw do

  resources :qoutes, only: [] do
    collection do
      get 'import'
      get 'random'
    end
  end

  get 'home/index'
  root 'home#index'
end
