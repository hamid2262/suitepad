class CreateQoutes < ActiveRecord::Migration
  def change
    create_table :qoutes do |t|
      t.text :body
      t.string :author
      t.string :genre

      t.timestamps null: false
    end
  end
end
