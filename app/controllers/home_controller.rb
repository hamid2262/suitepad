class HomeController < ApplicationController
  before_action :check_qoutes

  def index
    @qoute = Qoute.get_qoute
    @background = Qoute.get_background
  end

  private

    def check_qoutes
      redirect_to import_qoutes_path if Qoute.all.blank?
    end
end
