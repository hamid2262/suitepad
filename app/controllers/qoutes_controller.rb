class QoutesController < ApplicationController

  def import
    Qoute.delete_all
    Qoute.csv_import
    redirect_to root_path
  end

  def random
    @background = Qoute.get_background
    @qoute = Qoute.get_qoute
  end

end
