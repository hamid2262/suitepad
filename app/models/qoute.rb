class Qoute < ActiveRecord::Base

  def self.get_qoute
    qoutes = Qoute.all
    rand_num = rand(qoutes.size)
    qoutes[rand_num]
  end

  def self.get_background
    pictures = flickr.photos.search(tags: "background")
    rand_num = rand(pictures.size)
    FlickRaw.url_b(pictures[rand_num])
  end

  def self.csv_import
    file_path = Rails.root.to_s + QOUTES_FILE_PATH
    qoutes = []
    CSV.foreach(file_path, headers: true, col_sep: ';') do |row|
      qoutes << Qoute.new(qoute_mapper(row))
    end
    Qoute.import qoutes
  end

  private
    def self.qoute_mapper(row)
      row_hash = row.to_hash
      {
        body: row_hash["QUOTE"],
        author: row_hash["AUTHOR"],
        genre: row_hash["GENRE"]
      }
    end
end
